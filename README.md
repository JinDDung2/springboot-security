# Spring Security & JWT 실습

## ❄️ 공식 문서

📗 Spring Security : https://docs.spring.io/spring-authorization-server/docs/current/reference/html/getting-started.html

📗 JWT : https://jwt.io/

## 구현기능


| 기능           |                api                |                                                Ex                                                |
|--------------|:---------------------------------:|:------------------------------------------------------------------------------------------------:|
| 회원가입         |    Post <br/>api/v1/users/join    |  |
| Exception 처리 |        |  |

